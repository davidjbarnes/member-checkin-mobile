import React, {Component} from 'react';
import { Modal, StyleSheet, View } from 'react-native';
import Camera from 'react-native-camera';
import SplashScreen from 'react-native-splash-screen';

export default class App extends Component {
  constructor() {
    super();

    this.state = {
      lastId:null,
      bgColor: '#F5FCFF'
    };
  }

  componentDidUpdate() {
  
  }

  componentWillMount() {
    
  }

  componentDidMount() {
    SplashScreen.hide();

    setInterval(() => {
      console.log("3 seconds is up. clearing lastId.");
      this.setState({
        lastId:null,
        bgColor: '#F5FCFF'
      });
    }, 3000);
  }

  onCodeRead = (e) => {
    if(this.state.lastId != e.data) {
      this.setState({
        lastId:e.data,
        bgColor: 'green'
      });
      
      fetch('https://member-checkin-api.herokuapp.com/?id='.concat(e.data)).then((response) => response.json()).then((responseJson) => {
        
      }).catch((error) => {
        console.error(error);
      });
    }
  }

  render() {
    return (
      <View style={styles.container} backgroundColor={this.state.bgColor}>
      <Modal
          animationType="fade"
          transparent={false}
          visible={this.state.bgColor=='green'}>
        </Modal>
        <Camera style={styles.camera}
            ref={cam => this.camera=cam}
            captureQuality={Camera.constants.CaptureQuality["720p"]}
            aspect={Camera.constants.Aspect.fill}
            onBarCodeRead={this.onCodeRead}></Camera>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    margin: 0,
    backgroundColor: "#dbdbdb"
  },camera: {
    flex: 1,
  }
});